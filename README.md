# Code exercise for Stratio
## Author: Christian Fortich Jassan

This repo contains a possible solution for the supermarket problem. It's written
in Scala (2.12) and features the use of Spark (2.4). The use of Spark is
motivated for the possibility of this process to be run with a large amount of
rows, in which Spark excels due to the transparent management of resources and
the possibility of distributing the work (maybe along a cluster). Feel free to
discuss with me any aspect of the code you find worth talking about.

This project's dependencies are managed with `sbt` and is divided in the
following classes:

- Products: It is the class where products are retrieved. In this very case,
products are added into a hardcoded dataframe, but it is expected that this
be responsible of loading data from a database
- Basket: This class takes input arguments and generates a basket dataframe that
contains the amount of bought products along with their unit prices
- Discount: This class takes a basket and calculates the applicable discounts to
said basket, using a specified logics that depend on the articles in the cart
- PriceBasket: Singleton that manages the calls to the methods in the above
classes.

Every class includes a set of unit tests to verify that the outputs are those
we expect when we change the input arguments.

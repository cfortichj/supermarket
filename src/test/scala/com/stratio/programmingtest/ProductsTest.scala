package com.stratio.programmingtest

import org.apache.spark.sql.SparkSession
import org.scalatest.{BeforeAndAfter, FunSuite}

class ProductsTest extends FunSuite with BeforeAndAfter {
  private var product: Products = _

  private var sparkSess: SparkSession = _

  before {
    // setting a hadoop home dir for machines where hadoop is not fully
    // installed
    System.setProperty("hadoop.home.dir", "C:\\hadoop")
    product = new Products
    sparkSess = SparkSession
      .builder()
      .appName("Products test")
      .master("local")
      .getOrCreate()
    sparkSess.sparkContext.setLogLevel("OFF")
  }

  test("getListOfProducts: Existing products should yield a " +
    "non-empty products df") {
    val args: Array[String] = Array("apples", "milk", "bread")
    val products = product.getListOfProducts(sparkSess, args)
    assert(products.count >= 0)
  }
  test("getListOfProducts: Non-existing products should yield a " +
    "non-null empty products df") {
    val args: Array[String] = Array("elem1", "elem2", "elem3")
    val products = product.getListOfProducts(sparkSess, args)
    assert(products.count == 0)
  }
  test("getListOfProducts: Null input should give a non-null " +
    "empty products df") {
    val args: Array[String] = null
    val products = product.getListOfProducts(sparkSess, args)
    assert(products.count == 0)
  }
  test("getListOfProducts: Null spark session should throw " +
    "NullPointerException ") {
    val args: Array[String] = Array("elem1", "elem2", "elem3")
    val thrown = intercept[NullPointerException] {
      product.getListOfProducts(null, args)
    }
    assert(thrown.isInstanceOf[NullPointerException])
  }
  test("getListOfProducts: Empty args should yield non-null empty " +
    "df") {
    val args: Array[String] = Array()
    val products = product.getListOfProducts(sparkSess, args)
    assert(products.count == 0)
  }

}

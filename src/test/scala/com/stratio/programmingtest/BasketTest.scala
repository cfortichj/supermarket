package com.stratio.programmingtest

import org.apache.spark.sql.SparkSession
import org.scalatest.{BeforeAndAfter, FunSuite}

class BasketTest extends FunSuite with BeforeAndAfter {
  private var basket: Basket = _
  private var sparkSess: SparkSession = _

  before {
    // setting a hadoop home dir for machines where hadoop is not fully
    // installed
    System.setProperty("hadoop.home.dir", "C:\\hadoop")
    basket = new Basket
    sparkSess = SparkSession
      .builder()
      .appName("Basket test")
      .master("local")
      .getOrCreate()
    sparkSess.sparkContext.setLogLevel("OFF")
  }

  test("generateBasket: Existing input should give a non-null " +
    "basket") {
    val args: Array[String] = Array("apples", "milk", "bread")
    val b = basket.generateBasket(args, sparkSess)
    assert(b.count >= 0)
  }
  test("generateBasket: Non-existing input should give a non-null " +
    "basket of size zero") {
    val args: Array[String] = Array("elem1", "elem2", "elem3")
    val b = basket.generateBasket(args, sparkSess)
    assert(b.count == 0)
  }
  test("generateBasket: Null input should give a non-null basket " +
    "of size zero") {
    val args: Array[String] = null
    val b = basket.generateBasket(args, sparkSess)
    assert(b.count == 0)
  }
  test("generateBasket: Null spark session should throw " +
    "NullPointerException ") {
    val args: Array[String] = Array("elem1", "elem2", "elem3")
    val thrown = intercept[NullPointerException] {
      basket.generateBasket(args, null)
    }
    assert(thrown.isInstanceOf[NullPointerException])
  }
  test("generateBasket: Empty input should give a non-null basket " +
    "of size zero") {
    val args: Array[String] = Array()
    val b = basket.generateBasket(args, sparkSess)
    assert(b.count == 0)
  }

}

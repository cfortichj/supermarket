package com.stratio.programmingtest

import org.apache.spark.sql.types._
import org.apache.spark.sql.{Row, SparkSession}
import org.scalatest.{BeforeAndAfter, FunSuite}

class DiscountTest extends FunSuite with BeforeAndAfter {
  lazy val sparkSess: SparkSession = SparkSession
    .builder()
    .master("local")
    .appName("Discount test")
    .getOrCreate()
  private var discount: Discount = _

  before {
    // setting a hadoop home dir for machines where hadoop is not fully
    // installed
    System.setProperty("hadoop.home.dir", "C:\\hadoop")
    discount = new Discount
    sparkSess.sparkContext.setLogLevel("OFF")
  }
  test("calculateDiscounts: Null SparkSession should throw " +
    "NullPointerException") {
    val thrown = intercept[NullPointerException] {
      discount.calculateDiscounts(null, sparkSess.emptyDataFrame)
    }
    assert(thrown.isInstanceOf[NullPointerException])
  }


  test("calculateDiscounts: Empty cart on non-null SparkSession " +
    "should yield non-null empty discount df") {
    // defining dataframe schema
    val schema = StructType(List(
      StructField("productName", StringType, nullable = true),
      StructField("unitPrice", DoubleType, nullable = true),
      StructField("count", DoubleType, nullable = true)
    ))
    // creating dataframe with given schema
    val emptyDF = sparkSess.createDataFrame(sparkSess.sparkContext
      .emptyRDD[Row], schema)

    val discounts = discount.calculateDiscounts(sparkSess, emptyDF)
    assert(discounts.count == 0)
  }

  test("calculateDiscounts: Non-empty cart should yield non-empty " +
    "discount DF") {
    import sparkSess.implicits._
    val products = Seq(
      ("soup", "0.65", "1"),
      ("bread", "0.8", "1"),
      ("milk", "1.3", "1"),
      ("apples", "1.0", "1"),
      ("peanuts", "2.0", "1"),
      ("banana", "1.2", "1")
    ).toDF("productName", "unitPrice", "count")

    val discounts = discount.calculateDiscounts(sparkSess, products)
    assert(discounts.count > 0)
  }
  test("getPercentDiscount: Null SparkSession should throw " +
    "NullPointerException") {
    val thrown = intercept[NullPointerException] {
      discount.getPercentDiscounts(null, Array())
    }
    assert(thrown.isInstanceOf[NullPointerException])
  }
  test("getPercentDiscount: null args on non-null SparkSession " +
    "should throw NullPointerException") {
    val thrown = intercept[NullPointerException] {
      discount.getPercentDiscounts(sparkSess, null)
    }
    assert(thrown.isInstanceOf[NullPointerException])
  }
  test("getPercentDiscount: empty non-null args on non-null " +
    "SparkSession should yield non-null empty dataframe") {

    val discounts = discount.getPercentDiscounts(sparkSess, Array())

    assert(discounts.count == 0)
  }
  test("getPercentDiscount: non-empty non-existing args on " +
    "non-null SparkSession should yield non-null empty dataframe") {

    val discounts = discount.getPercentDiscounts(sparkSess,
      Array("elem1", "elem2", "elem3"))

    assert(discounts.count == 0)
  }
  test("getPercentDiscount: non-empty args on non-null " +
    "SparkSession should yield non-null non-empty dataframe") {

    val discounts = discount.getPercentDiscounts(sparkSess,
      Array("apples", "milk", "bread"))

    assert(discounts.count != 0)
  }
  test("getPerItemDiscounts: Null SparkSession should throw " +
    "NullPointerException") {
    val thrown = intercept[NullPointerException] {
      discount.getPerItemDiscounts(null, Array())
    }
    assert(thrown.isInstanceOf[NullPointerException])
  }
  test("getPerItemDiscounts: null args on non-null SparkSession " +
    "should throw NullPointerException") {
    val thrown = intercept[NullPointerException] {
      discount.getPerItemDiscounts(sparkSess, null)
    }
    assert(thrown.isInstanceOf[NullPointerException])
  }
  test("getPerItemDiscounts: empty non-null args on non-null " +
    "SparkSession should yield non-null empty dataframe") {

    val discounts = discount.getPerItemDiscounts(sparkSess, Array())

    assert(discounts.count == 0)
  }
  test("getPerItemDiscounts: non-empty non-existing args on " +
    "non-null SparkSession should yield non-null empty dataframe") {

    val discounts = discount.getPerItemDiscounts(sparkSess,
      Array("elem1", "elem2", "elem3"))

    assert(discounts.count == 0)
  }
  test("getPerItemDiscounts: non-empty args on non-null " +
    "SparkSession should yield non-null non-empty dataframe") {

    val discounts = discount.getPerItemDiscounts(sparkSess,
      Array("apples", "milk", "bread"))

    assert(discounts.count != 0)
  }
}

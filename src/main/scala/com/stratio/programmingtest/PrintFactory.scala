package com.stratio.programmingtest

import org.apache.spark.sql.functions.sum
import org.apache.spark.sql.{DataFrame, SparkSession}

class PrintFactory {

  /**
    * This method is used to separate the reporting logic from the discount
    * logic, so it can be expanded to report, for instance, via email,
    * written logs, etc...
    *
    * @param sparkSess : Correctly initialized Spark session
    * @param subtotal  : Total before all discounts
    * @param discounts : Dataframe containing all kind of discounts
    */
  def printResults(sparkSess: SparkSession,
                   subtotal: Double,
                   discounts: DataFrame): Unit = {
    import sparkSess.implicits._

    // check if there's any applicable discount
    if (discounts != null && !discounts.take(1).isEmpty) {
      println(f"Subtotal: $subtotal%.2f")
      discounts.collect.foreach(row => {
        val discountMessage = row.getString(0)
        val discount = row.getDouble(1)
        println(f"${discountMessage.capitalize}$discount%.2f")
      })
      val totalDiscount = discounts
        .agg(sum($"discount"))
        .first
        .getDouble(0)

      println(f"Total: ${subtotal - totalDiscount}%.2f")
    }
    else {
      println(f"Subtotal: $subtotal%.2f (No offers available)")
      println(f"Total: $subtotal%.2f")
    }


  }

}

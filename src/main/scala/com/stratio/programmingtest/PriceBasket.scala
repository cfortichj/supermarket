package com.stratio.programmingtest

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.sum
import org.apache.spark.sql.types.{DoubleType, IntegerType}


object PriceBasket {

  def main(args: Array[String]): Unit = {

    // setting a hadoop home dir for machines where hadoop is not fully
    // installed
    System.setProperty("hadoop.home.dir", "C:\\hadoop")

    // here we are going to use spark in local mode. It may be a little
    // "overkill" for this task but it "future-proofs" it in case it is used
    // for more data
    val sparkSess = SparkSession
      .builder()
      .appName("Coding test for stratio")
      .master("local")
      .getOrCreate()

    sparkSess.sparkContext.setLogLevel("OFF")

    import sparkSess.implicits._

    // empty cart case
    if (args.length == 0) {
      (new PrintFactory).printResults(sparkSess, 0, null)
    }
    else {

      // here we calculate the amount of products bought by the client (this is
      // the parsed input arguments

      val basket = (new Basket).generateBasket(args, sparkSess)

      // here we get the subtotal. This is the price before discounts
      val subtotal = basket
        .select($"unitPrice".cast(DoubleType) * $"count".cast(IntegerType) as
          "subtotal")
        .agg(sum("subtotal"))
        .first()
        .getDouble(0)

      // here we calculate all discounts
      val discounts = (new Discount)
        .calculateDiscounts(sparkSess, basket)

      // here we print the results
      (new PrintFactory).printResults(sparkSess, subtotal, discounts)
    }

  }

}

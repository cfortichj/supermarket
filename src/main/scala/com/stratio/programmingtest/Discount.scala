package com.stratio.programmingtest

import org.apache.spark.sql.functions.{concat, lit}
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.{DataFrame, SparkSession}

class Discount {

  /**
    * This method is used to calculate all applicable discounts within a
    * given cart. Currently there are two kind of discounts but it can be
    * expanded to make place for as much discounts as possible. Discount
    * dataframes are hardcoded here but are expected to be loaded from an
    * external source
    *
    * @param cart      : Contains a dataframe with the products bought along with
    *                  their quantities and unit prices
    * @param sparkSess : A correctly initialized spark session
    * @return dataframe containing all applicable discounts
    */
  def calculateDiscounts(sparkSess: SparkSession, cart: DataFrame)
  : DataFrame
  = {
    import sparkSess.implicits._

    val args = cart
      .select($"productName")
      .map(row => row.getString(0))
      .collect

    // here we get all kind of discounts
    val percentDiscounts = getPercentDiscounts(sparkSess, args)
    val discountPerItemBuy = getPerItemDiscounts(sparkSess, args)

    // here we calculate applicable discount for percent discounts
    val applicableDiscounts1 = percentDiscounts.join(cart,
      percentDiscounts("product") === cart("productName"),
      "inner")
      .select(concat($"product",
        lit(" "), $"percentageDiscount", lit("% off: "
        )) as "discountMessage",
        $"percentageDiscount".cast(DoubleType) / 100 *
          $"unitPrice".cast(DoubleType) *
          $"count".cast(DoubleType) as "discount")

    // here we calculate applicable discount per "buy-x-get-y" discounts
    val applicableDiscounts2 = cart
      .join(discountPerItemBuy,
        discountPerItemBuy("itemToBuy") === cart("productName") &&
          discountPerItemBuy("amountToBuy").leq(cart("count")),
        "inner")
      .select($"amountToBuy", $"itemToBuy", $"discountedItem", $"percent")
      .join(cart,
        cart("productName") === discountPerItemBuy("discountedItem"),
        "inner")
      .select(concat($"amountToBuy", lit(" "), $"itemToBuy",
        lit(" get "), $"percent", lit("% "), $"discountedItem",
        lit(": ")),
        $"percent" / 100 * $"unitPrice")

    // we return all the applicable discounts
    applicableDiscounts1
      .union(applicableDiscounts2)
  }

  def getPercentDiscounts(sparkSess: SparkSession, args: Array[String])
  : DataFrame = {
    import sparkSess.implicits._
    val percentDiscounts = Seq(
      ("apples", "10") /*,
      ("banana", "30")*/
    ).toDF("product", "percentageDiscount")

    percentDiscounts.where($"product".isin(args: _*))
  }

  def getPerItemDiscounts(sparkSess: SparkSession, args: Array[String])
  : DataFrame = {
    import sparkSess.implicits._
    val discountPerItemBuy = Seq(
      ("soup", "2", "bread", "50") /*,
      ("milk", "1", "peanuts", "20")*/
    ).toDF("itemToBuy", "amountToBuy", "discountedItem", "percent")

    discountPerItemBuy
      .where($"itemToBuy".isin(args: _*)
        or $"discountedItem".isin(args: _*))
  }
}

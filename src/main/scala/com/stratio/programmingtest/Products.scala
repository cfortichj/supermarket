package com.stratio.programmingtest

import org.apache.spark.sql.{DataFrame, SparkSession}

class Products {

  def getListOfProducts(sparkSess: SparkSession, args: Array[String])
  : DataFrame = {
    import sparkSess.implicits._

    // here we define a dataframe with the whole list of products. Again, if
    // this is to be bigger, this could be loaded from a file or a Hive
    // table, for instance
    val products = Seq(
      ("soup", "0.65", "pound"),
      ("bread", "0.8", "pound"),
      ("milk", "1.3", "pound"),
      ("apples", "1.0", "pound"),
      ("peanuts", "2.0", "euro"),
      ("banana", "1.2", "euro")
    ).toDF("productName", "productPrice", "productCurrency")

    // selecting only relevant products from the whole product database
    // here we calculate the amount of products bought by the client (this is
    // the parsed input arguments
    if (args == null || args.length == 0) {
      sparkSess.emptyDataFrame
    }
    else {
      products.where($"productName".isin(args: _*))
    }
  }

}

package com.stratio.programmingtest

import org.apache.spark.sql.functions.when
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.{DataFrame, SparkSession}

class Basket {
  def generateBasket(args: Array[String], sparkSess: SparkSession): DataFrame = {
    import sparkSess.implicits._

    // here we calculate the amount of products bought by the client (this is
    // the parsed input arguments
    if (args == null || args.length == 0) {
      sparkSess.emptyDataFrame
    }
    else {
      val products = (new Products).getListOfProducts(sparkSess, args)
      val boughtProducts = args
        .map(prod => prod.toLowerCase)
        .toSeq
        .toDF("boughtProducts")
        .groupBy($"boughtProducts")
        .count

      // here we "translate" all the currencies in euros since final output is
      // required to be in euros
      val prodInEuro = products
        .withColumn("productPrice",
          when($"productCurrency" === "pound",
            $"productPrice".cast(DoubleType) / 0.9)
            .otherwise($"productPrice"))
        .select($"productName", $"productPrice")

      // here we join both dataframes to get a new one containing unit prices
      // and amounts bought in the current ticket
      boughtProducts
        .join(prodInEuro, boughtProducts("boughtProducts") === prodInEuro
        ("productName"), "inner")
        .select($"productName", $"productPrice" as "unitPrice", $"count")
    }
  }
}
